package Exercici3;

import Keyboard.Keyboard;

public class Exercici3 {

	public static void main(String[] args) {
		
		double tempsTerra, tempsLluna;
		double trencatTerra, trencatLluna;
		System.out.println();
		System.out.println("TEMPS DE CAIGUDA D'UN S�LID (sensefregament)");
		System.out.println("----- -- ------- ---- -----");
		System.out.println();
		System.out.print("Al�ada inicial (en km): ");
		double hEnKm = Keyboard.readDouble();
		hEnKm *= 1000;
		trencatTerra = 2 * hEnKm / 9.1;
		trencatLluna = 2 * hEnKm / 1.6;
		tempsTerra = Math.sqrt(trencatTerra);
		tempsLluna = Math.sqrt(trencatLluna);
		tempsTerra = Math.round(tempsTerra);
		tempsLluna = Math.round(tempsLluna);
		System.out.println();
		System.out.print("Un objecte llen�at des de ");
		System.out.print(hEnKm);
		System.out.println(" metres");
		System.out.print("trigar� en arribar a la superf�cie ");
		System.out.print(tempsTerra);
		System.out.println(" segons, si �s a la Terra");
		System.out.print("per� a la lluna trigar� ");
		System.out.print(tempsLluna);
		System.out.println(" segons.");
		System.out.println();
		System.out.println();
		
	}
	
}
