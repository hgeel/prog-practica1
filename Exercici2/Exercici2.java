package Exercici2;

import Keyboard.Keyboard;

public class Exercici2 {

	public static void main(String[] args) {
		
		System.out.println("Calculadora d'arees de triangles.");
		System.out.print("Introdueix la base: ");
		double base = Keyboard.readDouble();
		System.out.print("Introdueix l'altura: ");
		double altura = Keyboard.readDouble();
		System.out.println();
		
		double area = base * altura / 2;
		
		System.out.println("Base: " + base + "cm");
		System.out.println("Altura: " + altura + "cm");
		System.err.println("Area del triangle: " + area + "cm^2");
		
	}
	
}
