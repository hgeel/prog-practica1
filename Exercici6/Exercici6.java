package Exercici6;

import Keyboard.Keyboard;

public class Exercici6 {
	
	public static void main(String[] args) {
		
		System.out.println("Especifica el n�mero de segons");
		System.out.println("------------------------------");
		
		System.out.print("Segons: ");
		int total = Keyboard.readInt();
		
		int hores = (total / 60) / 60;
		int minuts = (total / 60) % 60;
		int segons = total % 60;
		
		System.out.println(total + " sec s�n: " + hores + " hores, " + minuts + " minuts i " + segons + " segons.");
		
	}

}
