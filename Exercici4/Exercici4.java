package Exercici4;

public class Exercici4 {

	public static void main(String[] args) {
		
		System.out.println("MIDES D'UNA PANTALLA DE TELEVISOR");
		System.out.println("----- ----- -------- -- ---------");
		System.out.println("");
		System.out.print("Quina �s la mida en polzades de la diagonal? ");
		
		double diagonal = Keyboard.Keyboard.readDouble();
		
		System.out.println("\nUn televisor de "
				+ diagonal + " polzades ("
				+ (diagonal * 2.54) + " cent�metres)");
		
		double c_petit = diagonal * 0.49;
		double c_gran = c_petit * 16 / 9;
		
		System.out.println("t� un costat petit de "
				+ c_petit + " polzades ("
				+ Math.round(c_petit * 2.54) + " cent�metres)");
		
		System.out.println("i un costat gran de "
				+ c_gran + " polzades ("
				+ Math.round(c_gran * 2.54) + " cent�metres)");
		
	}
	
}
