package Exercici5;

import Keyboard.Keyboard;

public class Exercici5 {

	public static void main(String[] args) {
		
		System.out.println("Especifica l'edat de les 4 persones");
		System.out.println("-----------------------------------\n");
		
		System.out.print("Persona 1 - Quants anys t�? ");
		int e1 = Keyboard.readInt();
		
		System.out.print("Persona 2 - Quants anys t�? ");
		int e2 = Keyboard.readInt();
		
		System.out.print("Persona 3 - Quants anys t�? ");
		int e3 = Keyboard.readInt();
		
		System.out.print("Persona 4 - Quants anys t�? ");
		int e4 = Keyboard.readInt();
		
		double mitjana = (double) (e1 + e2 + e3 + e4) / 4;
		System.out.println("\nL'edat mitjana �s: " + mitjana);
		
	}
	
}
